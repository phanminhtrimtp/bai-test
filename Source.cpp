#include <iostream>
#include <string>
#include<string.h>
using namespace std;

void hoanvi(char** a, char** b)
{
	char* temp = *a;
	*a = *b;
	*b = temp;
}

char* addBigNumber(char* stn1, char* stn2)
{
	if (strlen(stn1) < strlen(stn2))
	{
		hoanvi(&stn1, &stn2);
	}

	size_t chieudai1 = strlen(stn1), chieudai2 = strlen(stn2);
	char* result = new char[chieudai1 + 2];
	memset(result, '0', chieudai1);
	bool sonho = false;
	int i = 0;

		for (i = 0; i < chieudai2; i++)
		{
			int tong = stn2[chieudai2 - i - 1] - '0' + stn1[chieudai1 - i - 1] - '0';
			
			if (sonho)
				tong++;
			printf("Buoc %d: Ta lay %d cong voi %d va cong cho so nho la %d duoc %d ghi %d nho %d \n", i + 1, stn1[chieudai1 - i - 1] - '0', stn2[chieudai2 - i - 1] - '0', sonho, tong, tong % 10, tong / 10);
			sonho = tong > 9;

			tong = tong % 10;

			result[chieudai1 - i] = tong + '0';
			
		}
	

		for (int y = 0; y < chieudai1 - chieudai2; y++)
		{
			int tong = stn1[chieudai1 - chieudai2 - y - 1] - '0';

			if (sonho)
				tong++;
			printf("Buoc %d: Ta lay %d cong voi %d va cong cho so nho la %d duoc %d ghi %d nho %d \n", i + 1, stn1[chieudai1 - chieudai2 - y - 1] - '0', 0, sonho, tong, tong % 10, tong / 10);
			sonho = tong > 9;

			tong = tong % 10;

			result[chieudai1 - chieudai2 - y] = tong + '0';
			
		}
	

	result[chieudai1 + 1] = '\0';

	if (sonho)
	{
		result[0] = '1';
	}
	else
	{
		for (int i = 0; i <= chieudai1; i++)
			result[i] = result[i + 1];
	}

	return result;
}

int main()
{
	string stn1 = "78949";
	string stn2 = "4578";
	string result = addBigNumber(&stn1[0], &stn2[0]);
	cout << "Ket qua phep cong: " << result;
	return 0;
}
